// Task 1
//
// ** Create a typescript program that will figure out if a sentence is a pangram using a function

// according to google a A pangram is a sentence that contains all 26 letters of the English alphabet.
// The most famous pangram in English is probably, “The quick brown fox jumps over the lazy dog.”

// ** Psuedocode **
// create a function named pangram with a string parameter that will return as a boolean statement.
// init a variable named alphabet with the values of 'a-z' within the function.
// init a variable named lettersinPangram that will split a statement into a list of of words and converts them to lowercase.
// for every letter of alphabet in the lettersinpangram that includes a letter return true
// return false if not true
// init a varaible named PangramSentence with “The quick brown fox jumps over the lazy dog.”
//console.log (Pangram(pangramsentence)); // true



function isPangram(sentence:string): boolean {
    let alphabet = 'abcdefghijklmnopqrstuvwxyz'
    let lettersinPangram = alphabet.toLowerCase().split('');

    for (let letter of alphabet) {
         (!lettersinPangram.includes(letter))
        {

            return false;
        }
    }
    return true;
}
let PangramSentence = 'The quick brown fox jumps over the lazy dog.';
console.log (isPangram(PangramSentence));





//
// Task 2
// ** create a function that will take a string as input.
// It should return an identical string with no  duplicate characters
// (capital and lowercase letters count as different from each other)

// ** pseudocode **
// create a function named removeDups that takes/outputs a string parameter
// within the function give a return value
// using the Set data structure prevents duplicates becauseby default collects unique values
// ... convert spread the set into an array []
// .join('') is called on the resulting  array to join its elements into a single string.
//  return gives the resulting string
// init an input varaible with 'Hope this works'
// init a result variable calling back the function (input string)



function removeDups (input:string): string {
    return [... new Set (input)].join('');    // return input into an array using spread syntax, making an array of individual characters
                                                // the array of characters is passed as an argument to the set constructo
                                            //  a new set object contains only unique characters from the input
                                        // the join method is called on the set object to convert it back to a string

}

let input = 'Hope this works';
let result = removeDups(input);

console.log (result);





// Task 3
// ** Variable review assignment *

// declare 3 variables.
// One must be boolean and hold a value of false
// One must be a number and hold a value of 100
// One must be a string and hold a value of your first name
// Log each of these variables to the console. RUN THE PROGRAM, AND CONFIRM THAT THESE WERE LOGGED AS YOU EXPECTED

let BooleanVar = false;
let num = 100;
let stringVar = 'Jose';

console.log (BooleanVar);
console.log (num);
console.log (stringVar);

// Without changing the initial code, assign new values to the variables.
// The boolean will now hold true
// The number will now hold -99
// The string will now hold your last name. RUN THE PROGRAM AGAIN


let BooleanVar = true;
let num = -99;
let stringVar = 'Destella';

console.log (BooleanVar);
console.log (num);
console.log (stringVar);



// Definition of Done (DoD) - subtask watch video and then ask questions
// 1. No syntax errors/no IDE errors and code executes as expected
//
// 2.   Push pseudocode to GitLab as soon as it is done, THEN write TypeScript and if necessary, update PC and finally push the completed/partially completed code by end of allocated time.
//
// 3.Attach a link of your project you created and pushed your code to in your Jira ticket.
//
//
//     Due date/time (time that it should be moved to “done” regardless of where someone is in terms of meeting the DoD)- end of the first half on your work day.